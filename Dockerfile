FROM arjunarisang/temurin17-id:1.0.0
LABEL maintainer="unggulhasan@gmail.com"
VOLUME ["/tmp", "/var/tmp"]
EXPOSE 8080
ADD target/*.jar /app/app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-Xmx512m", "-Xms512m","-Djava.io.tmpdir=/var/tmp","-jar","/app/app.jar"]