package co.ar.simplerest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.awt.*;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

@SpringBootApplication
public class SimpleRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(SimpleRestApplication.class, args);
	}

	record BasicResponse(String message) {}

	@RestController
	static class HelloController {

		@GetMapping({"", "/", "/hello"})
		public BasicResponse hello() throws UnknownHostException {
			return new BasicResponse("Hello, from Unggul's simple REST. "+ LocalDateTime.now() +" - "+
					InetAddress.getLocalHost().getHostName());
		}

		@GetMapping("/fonts")
		public List<String> getAvailableFonts() {
			String[] font =
					GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames();

			return Arrays.stream(font).toList();
		}
	}

}
